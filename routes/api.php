<?php

use Illuminate\Support\Facades\Route;

Route::namespace('App\Http\Controllers')->group(function () {
    Route::post('register', 'API\UserController@store');
    Route::post('login', 'API\Auth\LoginController@Login');
});

Route::namespace('App\Http\Controllers')->middleware(['authenticatedUser'])->group(function () {
    Route::post('search', 'API\SearchController@index')->name("search.index");
    Route::post('offer', 'API\OfferController@index')->name("offer.index");
    Route::get('offer/getByEstate/{estateId}', 'API\OfferController@getByEstate')->name("offer.getByEstate");

    /*Start Routes Users*/
    Route::get('users', 'API\UserController@index');
    Route::get('user/{id}', 'API\UserController@show');
    Route::post('user/{id}', 'API\UserController@update');
    Route::post('user/{id}/realestate', 'API\UserController@allUserRealEstate');
    /*End Routes Users*/

    /*Start Routes RealEstate*/
    Route::get('realestate/{id}', 'API\EstateController@show');
    Route::post('realestates', 'API\EstateController@index');
    Route::post('AllRealEstates', 'API\EstateController@all');
    Route::post('realestate/user/{id}', 'API\EstateController@store');
    Route::post('realestate/{id}/user', 'API\EstateController@update');
    Route::post('myRealEstate/{id}','API\EstateController@myRealEstate');
    /*End Routes RealEstate*/

    /*start Route city*/
    Route::get('cities', 'API\CityController@getAllCities');
    /*end Route city*/

    /*start Route area*/
    Route::post('areas', 'API\AreaController@getAllAreas');
    /*end Route area*/

    /*start Route type*/
    Route::get('types', 'API\TypeController@getAllType');
    /*end Route type*/

    /*start Route Registries*/
    Route::get('Registries', 'API\RegistriesController@getAllRegistries');
    /*end Route Registries*/

    /*start Route Auth*/
    Route::group(['middleware' => ['jwt.verify']], function () {
        Route::get('user', 'API\UserController@getAuthenticatedUser');
    });
    /*End Route Auth*/
});
