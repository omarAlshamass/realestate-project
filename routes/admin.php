<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\EstateController;
use App\Http\Middleware\CheckAdmin;

//#################### Route to Admin/DashboardController ###################
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware([CheckAdmin::class]);  //->middleware('auth');

//################# Route to Admin/Auth/LoginController it is manage to operate inside site(secure) ##########
Route::group([], function () {
    Route::get('/login', [LoginController::class, 'index']);
    Route::post('/login', [LoginController::class, 'login'])->name('login');
    Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
});

//################### Route to Admin/UserController to manage users the site ####################
Route::group(['prefix' => 'users', 'middleware' => [CheckAdmin::class]], function () { //middleware=>'check.admin'
    Route::get('/{order}', [UserController::class, 'index'])->name('users.index');
    Route::post('/store', [UserController::class, 'store'])->name('users.store');
    Route::get('/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
    Route::post('/update{id}', [UserController::class, 'update'])->name('users.update');
    Route::get('/destroy/{id}', [UserController::class, 'destroy'])->name('users.destroy');
});

Route::group(['prefix' => 'estate', 'middleware' => [CheckAdmin::class]], function () {
    Route::get('/approved', [EstateController::class, 'getApproved'])->name('estate.approved');
    Route::get('/rejected', [EstateController::class, 'getRejected'])->name('estate.rejected');
    Route::get('/pending', [EstateController::class, 'getPending'])->name('estate.pending');
    Route::get('/show/{estateId}', [EstateController::class,'show'])->name('estate.show');
    Route::post('/accept/{estateId}',[EstateController::class,'setAccept'])->name('estate.accept');
    Route::post('/not/accept/{estateId}',[EstateController::class,'setNotAccept'])->name('estate.not.accept');
    // Route::get('/reject/{estateId}', [EstateController::class, 'rejectEstate'])->name('estate.rejectEstate');
});

Route::get('/create', [UserController::class, 'create']);
