<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords"
          content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <title>dashboard</title>
    <!-- Begin Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">
    <!-- End Fonts -->

    <!-- Begin icons -->
    <link rel="stylesheet" href="{{url('admin/fonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link href="{{url('https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
    <!-- End icons -->

    <!-- Begin Animation -->
    <link rel="stylesheet" type="text/css" href="{{url('admin/css-rtl/animate/animate.css')}}">
    <!-- End Animation -->

    <!-- Begin Style Notification -->
{{--    <link rel="stylesheet" href="{{url('admin/css-rtl/notification.css')}}">--}}
    <!-- End Style Notification -->

    <!-- BEGIN Navbar && Menu CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('admin/css-rtl/vendors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('admin/css-rtl/vertical-menu.css')}}">
    <!-- END Navbar && Menu CSS-->

    <!-- BEGIN Application(Dashboard) CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('admin/css-rtl/app.css')}}">
    <link rel="stylesheet" href="{{url('admin/css-rtl/style.css')}}">
    <!-- END Application(Dashboard) CSS-->

    <style>
        body {
            font-family: 'Cairo', sans-serif;
        }
    </style>
</head>
{{--<!-- @if(Request::is('admin/users/tickets/reply*')) chat-application @endif -->--}}
<body class="vertical-layout vertical-menu 2-columns fixed-navbar stop-scroll-x"
      data-open="click" data-menu="vertical-menu" data-col="2-columns">
<!-- fixed-top-->

<!--Begin Header -->
@include('layouts.admin.includes.nav')
<!--End Header -->

<!-- Begin SideBar-->
@include('layouts.admin.includes.menu')
<!--End Sidebar-->

{{--########### Begin Content ##############--}}
@yield('content')
{{--############ End Content ##############--}}


<script src="{{url('admin/js/jquery-3.5.1.min.js')}}"></script>

<!-- Begin Navbar & Menu JS-->
<script src="{{url('admin/js/vendors.min.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/app-menu.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/app.js')}}" type="text/javascript"></script>
<script src="{{url('admin/js/script.js')}}" type="text/javascript"></script>
<!-- End Navbar & Menu JS-->

@yield('script')

</body>
</html>

