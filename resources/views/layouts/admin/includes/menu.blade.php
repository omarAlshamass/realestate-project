<div class="main-menu menu-fixed menu-light menu-shadow" id="menu" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item active">
                <a href="{{route('dashboard')}}">
                    <i class="la la-mouse-pointer"></i>
                    <span class="menu-title" data-i18n="nav.add_on_drag_drop.main">الرئيسية</span>
                </a>
            </li>

            <li class="nav-item">
                <a href="#">
                <i class="la la-male"></i>
                <span class="menu-title" data-i18n="nav.dash.main">المستخدمين</span>
                <span class="badge badge-warning badge-pill float-right mr-2">{{App\Models\User::count()}}</span>
                </a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{route('users.index','asc')}}" data-i18n="nav.dash.ecommerce"> عرض الكل </a>
                    </li>

                    <li>
                        <a class="menu-item" href="{{url('/create')}}">إضافة مستخدم</a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a href="#">
                <i class="fa fa-picture-o"></i>
                <span class="menu-title" data-i18n="nav.dash.main">العقارات</span>
                <span class="badge badge-warning badge-pill float-right mr-2">{{App\Models\Estate::count()}}</span>
                </a>

                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{route('estate.approved')}}" data-i18n="nav.dash.ecommerce"> عرض الكل </a>
                    </li>

                </ul>
            </li>
        </ul>
    </div>
</div>
