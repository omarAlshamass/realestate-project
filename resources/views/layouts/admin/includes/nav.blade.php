    <!-- Begin Header -->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-light bg-primary navbar-shadow">
    <div class="navbar-wrapper">
{{--########################### Nav Menu ################################--}}
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item">
                    <div class="navbar-brand">
                        <img class="image-size-65px" id="logo_menu" alt="modern admin logo" src="{{url('admin/images/logo/Realestate_250_125.png')}}">
                    </div>
                </li>

                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i
                            class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>

        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">

{{--########################### Nav Right ################################--}}
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block" id="btn-menu">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                            <i class="ft-menu"></i>
                        </a>
                    </li>
{{--                    <li class="nav-item d-none d-md-block">--}}
{{--                        <a class="nav-link nav-link-expand" href="#">--}}
{{--                            <i class="ficon ft-maximize"></i>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>

{{--############################ Nav Left ################################--}}
                <ul class="nav navbar-nav">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <span class="user-name text-bold-700">{{Auth::check()?Auth::user()->name:'Sohaib Estanbouly'}}</span>
                            <span class="avatar avatar-online">
                                <img  style="height: 35px;" src="{{url('admin/images/profiles/Sohaib_Avatar.jpg')}}" alt="avatar">
                            </span>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href=""><i class="ft-user"></i> تعديل الملف الشحصي </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('logout')}}"><i class="ft-power"></i> تسجيل الخروج</a>
                        </div>
                    </li>

{{--################################# notification ##########################################--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link nav-link-label" id="btn_notification" href="#">--}}
{{--                            <i class="ficon ft-bell"></i>--}}
{{--                            <span class="badge badge-pill badge-default badge-danger badge-default badge-up badge-glow">5</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </div>
    </div>
</nav>
    <!--End Header -->
