@extends('layouts.admin.dashboard')

@section('content')
<div class="p-2 mt-5 position-relative">
{{--###################### Start Search ##################--}}
    <div class="row position-absolute w-100" id="container_actions_search" style="top:-35px;">
            <div class="btn btn-primary offset-sm-1 offset-md-2  col-sm-2 mr-2 pt-2">خيارات البحث :</div>

            <div class="dropdown col-sm-2">
                <a class="btn btn-primary dropdown-toggle w-150 h-100 pt-2" href="#" role="button" data-toggle="dropdown">ترتيب حسب</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{url('/users/asc')}}">تصاعدي</a>
                    <a class="dropdown-item" href="{{url('/users/desc')}}">تنازلي</a>
                </div>
            </div>

            <form class="d-flex col-sm-6 col-md-4" action="{{route('users.index','asc')}}" method="GET" >
                <input type="text" value="{{request()->has('field_search')?request('field_search'):""}}" id="field_search" name="field_search" class="w-50 h-100 mr-100px form-control" placeholder="بحث">
                <input type="submit" class="btn btn-primary" value="بحث">
            </form>
            {{--######################### Start Pagination #####################--}}
            @if(isset($users))
                <div>
                    {{$users->links()}}
                </div>
            @endif
            {{--######################### End Pagination #######################--}}
    </div>

    @if(session()->get('msg'))
        <div class="row">
            <div class="alert alert-danger col-md-6 offset-md-4 mt-1">
                {{session()->get('msg')}}
            </div>
        </div>
    @endif
{{--###################### End Search ##################--}}

{{--##################### Start Records Table Users (Manage Users) ###############--}}
    <table class="table table-hover col-sm-12 col-md-12 mt-2 position-absolute" id="table_content_data_users_for_manage_users" style="left:0; max-width:1300px;">
        <thead>
        <tr>
            <th scope="col">الاسم</th>
            <th scope="col">البريد الإلكتروني</th>
            <th scope="col">رقم الموبايل</th>
            <th scope="col">صلاحيته</th>
            <th scope="col">تعديل</th>
            <th scope="col">حذف</th>
        </tr>
        </thead>
        <tbody style="background-color: white">
        @if(isset($users))
            @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->phone_number}}</td>
                <td>{{$user->is_admin==0?'مستخدم':'مسؤول'}}</td>
                <td><a href="{{route('users.edit',$user->id)}}"><i class="fa fa-edit font-size-24px"></i></a></td>
                <td><a href="{{route('users.destroy',$user->id)}}"><i class="fa fa-times font-size-24px"></i></a></td>
            </tr>
            @endforeach
        @endif

        @if(isset($filterUser))
            <tr>
                <td>{{$filterUser->name}}</td>
                <td>{{$filterUser->email}}</td>
                <td>{{$filterUser->phone_number}}</td>
                <td>{{$filterUser->is_admin==0?'مستخدم':'مسؤول'}}</td>
                <td><a href="{{route('users.edit',$filterUser->id)}}"><i class="fa fa-edit font-size-24px"></i></a></td>
                <td><a href="{{route('users.destroy',$filterUser->id)}}"><i class="fa fa-times font-size-24px"></i></a></td>
            </tr>
        @endif
        </tbody>
    </table>
{{--##################### End Records Table Users ###############--}}

</div>
@stop
