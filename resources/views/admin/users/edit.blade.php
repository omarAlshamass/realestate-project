@extends('layouts.admin.dashboard')

@section('content')
    <section class="content p-3">
        <form method="POST" action="{{route('users.update',$user->id)}}">
            @csrf
            <div class="form-group">
                <label for="name">اسم المستخدم:</label>
                <input value="{{old('name',$user->name)}}" type="text" class="form-control" id="name" name="name" placeholder="اسم المستخدم">
            </div>

            <div class="row">
                <div class="form-group col-6">
                    <label for="email">البريد الإلكتروني:</label>
                    <input type="text" value="{{old('email',$user->email)}}" class="form-control" id="email" name="email" placeholder="البريد الإلكتروني">
                    <small class="form-text">لا تشاركه مع أي أحد.</small>
                </div>

                <div class="form-group col-6">
                    <label for="password">كلمة المرور:</label>
                    <input type="password" value="{{old('password',$user->password)}}" class="form-control" id="password" name="password" placeholder="كلمة المرور">
                </div>
            </div>

            <div class="form-group">
                <label for="specialty">صلاحياته:</label>
                <input type="radio" {{old('is_admin',$user->is_admin)==0?'checked':''}} name="is_admin" class="ml-4" value="0">مستخدم
                <input type="radio" {{old('is_admin',$user->is_admin)==1?'checked':''}} name="is_admin" class="ml-4" value="1">مسؤول
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label for="phone_number">رقم الموبايل:</label>
                    <input value="{{old('phone_number',$user->phone_number)}}" type="text" class="form-control" id="phone_number" name="phone_number" placeholder="رقم الموبايل">
                </div>
            </div>

            <div class="btn btn-group d-inline-block">
                <button class="btn btn-primary mt-2 ml-2" id="btn_store">إرسال</button>
                <button type="reset" class="btn btn-outline-primary mt-2 ml-2">حذف</button>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger m-0 mt-3 ml-5">
                    <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                    </ul>
                </div>
            @endif
        </form>
    </section>
@endsection
