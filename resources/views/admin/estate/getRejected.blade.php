@extends('layouts.admin.dashboard')

@section('content')
    <div class="p-2 mt-5 position-relative">
        {{--###################### Start Search ##################--}}
        <div class="row position-absolute w-100" id="container_actions_search" style="top:-35px;">
            <div class="btn btn-primary offset-sm-1 offset-md-2  col-sm-2 mr-2 pt-2">خيارات البحث :</div>

            <div class="dropdown col-sm-2">
                <a class="btn btn-primary dropdown-toggle w-150 h-100 pt-2" href="#" role="button" data-toggle="dropdown">ترتيب حسب</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{route('estate.approved')}}">النشط</a>
                    <a class="dropdown-item" href="{{route('estate.rejected')}}">الغير نشط</a>
                    <a class="dropdown-item" href="{{route('estate.pending')}}">المعلقة</a>
                </div>
            </div>

            {{--######################### Start Pagination #####################--}}
            @if(isset($users))
                <div>
                    {{$users->links()}}
                </div>
            @endif
            {{--######################### End Pagination #######################--}}
        </div>

        @if(session()->get('msg'))
            <div class="row">
                <div class="alert alert-danger col-md-6 offset-md-4 mt-1">
                    {{session()->get('msg')}}
                </div>
            </div>
        @endif
        {{--###################### End Search ##################--}}

        {{--##################### Start Records Table Users (Manage Users) ###############--}}
        <table class="table table-hover col-sm-12 col-md-12 mt-2 position-absolute" id="table_content_data_users_for_manage_users" style="left:0; max-width:1300px;">
            <thead>
            <tr>
                <th scope="col">رقم السجل</th>
                <th scope="col">نوع العقار</th>
                <th scope="col"> للبيع أم للإيجار</th>
                <th scope="col">حالة العقار</th>
                <th scope="col">تبديل الحالة</th>
            </tr>
            </thead>
            <tbody style="background-color: white">
           @if(isset($estates)||isset($type))
                @foreach($estates as $estate)
                        <tr id="clickable-row" onclick="getinfo(<?php echo $estate->id?>);">
                            <td>{{$estate->id}}</td>
                            <td>
                                @if($estate->realEstateType_id==1)
                                    {{'بيت'}}
                                @elseif($estate->realEstateType_id==2)
                                    {{'محل'}}
                                @else
                                    {{'أرض'}}
                                @endif
                            </td>
                            <td>{{$estate->rent_or_sale==0 ?'للبيع':'للإيجار'}}</td>
                            <td>
                                @if($estate->is_active==0)
                                    {{'غير نشط'}}
                                @elseif($estate->is_active==1)
                                    {{'نشط'}}
                                @else
                                    {{'معلق'}}
                                @endif
                            </td>
                            <td>
                                @if($estate->is_active==0)
                                    <form action="{{ url('estate/accept/'. $estate->id) }}" method="POST">
                                        @csrf
                                        <input type="submit" class="btn btn-success w-100 col-sm-12 col-md-2" value="نشط" style="max-width: 100px">
                                    </form>
                                @endif
                            </td>
                            
                        </tr>
               @endforeach
           @endif
            </tbody>
        </table>
        {{--##################### End Records Table Users ###############--}}
    </div>
@stop

@section('script')
    <script>
        // ####################### Events Click Rows Table ##################
        function getinfo(id){
            window.location = `https://projects.pro-apps.net/realestate-project/public/estate/show/${id}`;
        }
    </script>
@stop
