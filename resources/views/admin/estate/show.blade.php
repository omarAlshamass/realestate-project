<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>show details</title>
    <link rel="stylesheet" href="{{url('admin/css-rtl/bootstrap.css')}}">
    <link rel="stylesheet" href="{{url('admin/fontawesome_version_4/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{url('admin/fontawesome_version_4/css/all.css')}}">
    <link rel="stylesheet" href="{{url('admin/css-rtl/showEstate.css')}}">
</head>
<style>
    .stop-scroll-x {
        overflow-x: hidden;
    }
</style>

<body class="stop-scroll-x">
    @if(isset($estate) and isset($city) and isset($area) and isset($type) and isset($images) and isset($register))
    <!-- {{--    ########### images estate #############--}} -->

    <div class="slideshow-container mt-1 d-flex justify-content-center align-content-center position-relative">
        @foreach($images as $image)
        <div class="mySlides fade">
            <!-- <div class="numbertext">1/3</div> -->
            <img src="{{$image->url}}" class="image-size-400px w-100">
        </div>
        @endforeach
        <a class="prev position-absolute" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next position-absolute" onclick="plusSlides(1)">&#10095;</a>

    </div>
    <br>
    <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
    </div>

    <!-- {{--    ########## information estate ########--}} -->
    <div class="container mt-2 position-relative h-300px" id="container_information_estate">
    <div class="row w-100 p-sm-0 text-danger text-center" style="margin-left: 250px">
        <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2 bg-white shadow-silver border-radius-10 information"><i class="fa fa-home font-size-24px"></i><p>{{$type->name}}</p></div>
        <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2 bg-white shadow-silver border-radius-10 information"><i class="fa fa-map-marker font-size-24px"></i><p>{{$area->name}}</p></div>
        <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2 bg-white shadow-silver border-radius-10 information"><i class="fa fa-map font-size-24px"></i><p>{{$city->name}} </p></div>
        <div class="col-lg-4"></div>
        <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2  bg-white shadow-silver border-radius-10 information"><i class="fa fa-newspaper font-size-24px"></i><p>{{$estate->rent_or_sale==0 ?'للبيع':'للإيجار'}} </p></div>
        <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2  bg-white shadow-silver border-radius-10 information"><i class="fa fa-credit-card font-size-24px"></i><p>{{$estate->price}}</p></div>
        <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2  bg-white shadow-silver border-radius-10 information"><i class="fa fa-balance-scale  font-size-24px"></i><p>{{$register->name}} </p></div>
        <div class="col-lg-4"></div>
        @if($estate->rent_or_sale==1)
            <div class="col-sm-12 col-lg-2 p-2 ml-2 mb-2  bg-white shadow-silver border-radius-10 information"><i class="fa fa-sticky-note font-size-24px"></i><p>{{$estate->number_month}}</p></div>
        @endif
    </div>

        @if($estate->is_active==null)
        <div class="row w-100 d-flex justify-content-center mt-2 mr-min-200px" id="container_btn_accept_or_refusal">
            <form action="{{ url('estate/accept/'. $estate->id) }}" method="POST">
                @csrf
                <input type="submit" class="btn btn-success col-sm-12 col-md-2 mr-3 ml-3" value="قبول" style="max-width: 400px;">
            </form>
            <form action="{{ url('estate/not/accept/'. $estate->id) }}" method="POST">
                @csrf
                <input type="submit" class="btn btn-danger w-100 col-sm-12 col-md-2" value="رفض" style="max-width: 400px; margin-left:100px">
            </form>
        </div><br>
    @endif

        <script src="{{url('https://code.jquery.com/jquery-3.5.1.js')}}" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous">
        </script>

        <script src="{{url('admin/js/showEstate.js')}}"></script>
        @endif
</body>

</html>