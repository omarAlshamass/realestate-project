<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    {{--    ############ Styles #############--}}
    <link rel="stylesheet" href="{{url('admin/css-rtl/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('admin/css-rtl/scrolling.css')}}">
    <link rel="stylesheet" href="{{url('admin/fontawesome_version_4/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{url('admin/fontawesome_version_4/css/all.css')}}">
    <link rel="stylesheet" href="{{url('admin/auth/css/styles.css')}}">
</head>
<body class="stop-scroll-x">
<div class="form bg-white h-600 mt-5 position-relative mb-200">
    <div id="container_form_login" class="section-left w-50 d-flex justify-content-center">
        <form id="form-sign_in" class="col-sm-12 col-md-8 mt-50" action="{{route('login')}}" method="POST">
            @csrf
            <h1 class="text-center text-red_1 font-weight-bold mb-4">Login</h1>

            <div class="d-flex justify-content-center">
{{--                <a href="#"><i class="icon fab fa-facebook-f"></i></a>--}}
                <div class="container-icon mr-4"></div>
{{--                <a href="#"><i class="icon fab fa-google"></i></a>--}}
                <div class="container-icon mr-4"></div>
{{--                <a href="#"><i class="icon fab fa-instagram"></i></a>--}}
                <div class="container-icon mr-4"></div>
            </div><br>
            <p class="text-center text-secondary pr-3">our use your email account<p>

            <div class="input-group-prepend mt-5">
                <span class="input-group-text bg-light hidden-border-right"><i class="fa fa-envelope-open"></i></span>
                <input type="email" class="form-control bg-light col-sm-12 col-md-12 hidden-border-left" name="email" placeholder="Enter Email">
            </div><br>

            <div class="input-group-prepend">
                <span class="input-group-text bg-light hidden-border-right"><i class="fa fa-unlock-alt"></i></span>
                <input type="password" class="form-control bg-light col-sm-12 col-md-12 hidden-border-left" name="password" placeholder="Enter Password">
            </div><br>

{{--            <a href="#" class="text-center">Forget Your Password ?</a><br><br>--}}

            @if(session()->get('failsLogin'))
                <div class="alert alert-danger text-right">
                    {{session()->get('failsLogin')}}
                </div>
            @endif

            <div class="d-flex justify-content-center mt-2"><input type="submit" class="btn bg-red_1 pl-5 pr-5 mt-3 text-light border-radius-15" value="LOGIN"></div>
        </form>
    </div>

    <div class="part_eyebrow_to_form bg-gradient-red h-600" style="right: 10px;">
    </div>

    <div id="part_eyebrow_to_form_1">
        <div id="container_details_way_login_left" class="container mt-200 part_eyebrow_to_form" style="left: -700px;">
            <div class="align-items-center-grid">
                <h1 class="text-light text-center font-weight-bold mb-4">Hello, Friend!</h1>
                <p class="text-light text-center w-60 mb-4">Enter your personal details and you be want change direction press here</p>
                <button class="btn btn-danger w-30 border-light" id="btn_change_1" style="background-color:inherit">Change</button>
            </div>
        </div>
    </div>

    <div id="part_eyebrow_to_form_2">
        <div id="container_details_way_login_right" class="container mt-200 part_eyebrow_to_form " style="right: 10px; top:-500px; transition-duration: 1.5s;" >
            <div class="align-items-center-grid">
                <h1 class="text-light text-center font-weight-bold mb-4">Hello, Friend!</h1>
                <p class="text-light text-center w-60 mb-4">Enter your personal details and you be want change direction press here</p>
                <button class="btn btn-danger w-30 border-light" id="btn_change_2" style="background-color:inherit">Change</button>
            </div>
        </div>
    </div>
</div>
<script src="{{url('admin/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{url('admin/auth/js/scripts.js')}}"></script>
</body>
</html>
