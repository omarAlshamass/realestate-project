$(window).ready(function () {
// ##################### Events Logo layouts/admin/dashboard ##################
    $('#btn-menu').on('click',function () {
        if($('#menu').width()==259){
            $('#logo_menu').removeClass('image-size-150px').addClass('image-size-65px');
        }
        else if ($('#menu').width()==59){
            $('#logo_menu').removeClass('image-size-65px').addClass('image-size-150px');
        }
    });
    $(window).on('resize',function(){
        if($(window).width()>=768 && $(window).width()<=992) {
            $('#logo_menu').removeClass('image-size-150px').addClass('image-size-65px');
        }
    });

// ##################### Events table admin/users/index(show data users and manage ##################
    $('#btn-menu').on('click',function () {
        if($('#menu').width()==259){
            $('#table_content_data_users_for_manage_users').css("max-width","1300px");
        }
        else{
            $('#table_content_data_users_for_manage_users').css("max-width","1100px");
        }
    });

});

