$(document).ready(function(){
    $('#container_details_way_login_right').css("top","0");

    $('#btn_change_1').on('click',function () {
        $('#container_details_way_login_left').css({
            "left":"-700px",
            "visibility":"hidden",
        });
        $('#container_details_way_login_right').css({
            "right":"10px",
            "display":"block",
        });
        $('.part_eyebrow_to_form').css({
            "animation-name":"move_to_right",
            "animation-duration":"2s",
            "animation-fill-mode":"forwards"
        });
        $('#container_form_login').removeClass("section-right").addClass("section-left");
    });

    $('#btn_change_2').on('click',function () {
        $('#container_details_way_login_right').css({
            "display":"none",
        });
        $('#container_details_way_login_left').css({
            "left":"10px",
            "visibility":"visible",
        });
        $('.part_eyebrow_to_form').css({
            "animation-name":"move_to_left",
            "animation-duration":"2s",
            "animation-fill-mode":"forwards"
        });
        $('#container_form_login').removeClass("section-left").addClass("section-right");
    });
});
