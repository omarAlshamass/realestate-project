<?php

use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [
            ['id' => 1,'name' => 'دمشق القديمة','city_id' => 1,'code' => 'OLDDAMAS'],
            ['id' => 2,'name' => 'ساروجة','city_id' => 1,'code' => 'SAROGA'],
            ['id' => 3,'name' => 'القنوات','city_id' => 1,'code' => 'KANOWAT'],
            ['id' => 4,'name' => 'جوبر','city_id' => 1,'code' => 'JOBAR'],
            ['id' => 5, 'name' => 'الميدان','city_id' => 1,'code' => 'MEDAN'],
            ['id' => 6,'name' => 'الشاغور','city_id' => 1,'code' => 'SHAGHOR'],
            ['id' => 7,'name' => 'القدم','city_id' => 1,'code' => 'KADAM'],
            ['id' => 8,'name' => 'كفر سوسة','city_id' => 1,'code' => 'KAFARSOSA'],
            ['id' => 9,'name' => 'المزة','city_id' => 1,'code' => 'MAZA'],
            ['id' => 10,'name' => 'دمر','city_id' => 1,'code' => 'DAMR'],
            ['id' => 11,'name' => 'برزة','city_id' => 1,'code' => 'BARZA'],
            ['id' => 12,'name' => 'القابون','city_id' => 1,'code' => 'KABON'],
            ['id' => 13,'name' => 'ركن الدين','city_id' => 1,'code' => 'ROKNALDEEN'],
            ['id' => 14,'name' => 'الصالحية','city_id' => 1,'code' => 'SALEHEA'],
            ['id' => 15,'name' => 'المهاجرين','city_id' => 1,'code' => 'MHAJREEN'],
            // areas ref damas
            ['id' => 16,'name' => 'دوما','city_id' => 12,'code' => 'DOMA'],
            ['id' => 17,'name' => 'يبرود','city_id' => 12,'code' => 'EABROD'],
            ['id' => 18,'name' => 'النبك','city_id' => 12,'code' => 'NABEK'],
            ['id' => 19,'name' => 'التل','city_id' => 12,'code' => 'TAL'],
            ['id' => 20,'name' => 'القطيفة','city_id' => 12,'code' => 'KOTEFA'],
            ['id' => 21,'name' => 'الزبداني','city_id' => 12,'code' => 'ZABADANE'],
            ['id' => 22,'name' => 'ريف دمشق','city_id' => 12,'code' => 'REFDAMAS'],
            ['id' => 23,'name' => 'قطنا','city_id' => 12,'code' => 'KATANA'],
            ['id' => 24,'name' => 'داريا','city_id' => 12,'code' => 'DARAYA'],
            //areas halab
            ['id' => 25,'name' => 'العزيزية','city_id' => 2,'code' => 'ALAZEZIA'],
            ['id' => 26,'name' => 'سيف الدولة','city_id' => 2,'code' => 'SEFALDOLA'],
            ['id' => 27,'name' => 'الموكامبو','city_id' => 2,'code' => 'ALMOKAMBO'],
            ['id' => 28,'name' => 'الفرقان','city_id' => 2,'code' => 'ALFOROKAN'],
            ['id' => 29, 'name' => 'عفرين','city_id' => 2,'code' => 'AFREN'],
            ['id' => 30, 'name' => 'منبج','city_id' => 2,'code' => 'MANBEG'],
            //areas dar2a
            ['id' => 31,'name' => 'ازرع','city_id' => 3,'code' => 'AZRA2'],
            ['id' => 32,'name' => 'شيخ وسكين','city_id' => 3,'code' => 'SHE5WESKEN'],
            ['id' => 33,'name' => 'درعا البلد','city_id' => 3,'code' => 'DAR2AALBALAD'],
            ['id' => 34,'name' => 'حوران','city_id' => 3,'code' => 'HORAN'],
            ['id' => 35, 'name' => 'بصرة الحرير','city_id' => 3,'code' => 'BOSRAALHARRIR'],
            ['id' => 36,'name' => 'الصنمين','city_id' => 3,'code' => 'ALSANAMEN'],
            //areas der al zor
            ['id' => 37,'name' => 'دمشق القديمة','city_id' => 4,'code' => 'OLDDAMAS'],
            ['id' => 38,'name' => 'ساروجة','city_id' => 4,'code' => 'SAROGA'],
            ['id' => 39,'name' => 'القنوات','city_id' => 4,'code' => 'KANOWAT'],
            ['id' => 40,'name' => 'جوبر','city_id' => 4,'code' => 'JOBAR'],
            ['id' => 41,'name' => 'جوبر','city_id' => 4,'code' => 'JOBAR'],
            //areas hamah
            ['id' => 42,'name' => 'السلمية','city_id' => 5,'code' => 'ALSALAMEH'],
            ['id' => 43,'name' => 'مصياف','city_id' => 5,'code' => 'MISIAF'],
            ['id' => 44,'name' => 'سلحب','city_id' => 5,'code' => 'SALHAB'],
            ['id' => 45, 'name' => 'السقيلبية','city_id' => 5,'code' => 'SKILBAI'],
            ['id' => 46,'name' => 'محردة','city_id' => 5,'code' => 'MHARDA'],
            ['id' => 47,'name' => 'نيصاف','city_id' => 5,'code' => 'NESAF'],
            //areas hasaka
            ['id' => 48,'name' => 'قامشلي','city_id' => 6,'code' => 'KAMSHLE'],
            ['id' => 49,'name' => 'المالكية','city_id' => 6,'code' => 'ALMALKE'],
            ['id' => 50,'name' => 'رأس العين','city_id' => 6,'code' => 'RASALAEN'],
            //areas hose
            ['id' => 51,'name' => 'الرستن','city_id' => 7,'code' => 'ALRASTEN'],
            ['id' => 52,'name' => 'تدمر','city_id' => 7,'code' => 'TADMOR'],
            ['id' => 53,'name' => 'القصير','city_id' => 7,'code' => 'ALKOSER'],
            ['id' => 54,'name' => 'تلكلخ','city_id' => 7,'code' => 'TALKAL5'],
            ['id' => 55, 'name' => 'المخرم','city_id' => 7,'code' => 'ALMALKHAM'],
            //areas idleb
            ['id' => 56,'name' => 'معرة النعمان','city_id' => 8,'code' => 'MARAALNOMAN'],
            ['id' => 57,'name' => 'اريحة','city_id' => 8,'code' => 'AREHA'],
            ['id' => 58,'name' => 'جسر الشغور','city_id' => 8,'code' => 'JOSRALSHOGHOR'],
            ['id' => 59,'name' => 'معر تمصرين','city_id' => 8,'code' => 'MA2RTAMSAREN'],
            ['id' => 60, 'name' => 'حارم','city_id' => 8,'code' => 'HARM'],
            //areas lazkea
            ['id' => 61,'name' => 'جبلة','city_id' => 9,'code' => 'JABLA'],
            ['id' => 62,'name' => 'الحفة','city_id' => 9,'code' => 'ALHAFA'],
            ['id' => 63,'name' => 'القرداحة','city_id' => 9,'code' => 'ALKORDAHA'],
            //areas konetra
            ['id' => 64,'name' => 'فيق','city_id' => 10,'code' => 'FAEK'],
            //areas reka
            ['id' => 65,'name' => 'الثورة','city_id' => 11,'code' => 'OLDDAMAS'],
            ['id' => 66,'name' => 'تل ابيض','city_id' => 11,'code' => 'SAROGA'],
            //areas soida
            ['id' => 67,'name' => 'شهبا','city_id' => 13,'code' => 'SHAHBA'],
            //areas tartos
            ['id' => 68,'name' => 'الشيخ بدر','city_id' => 14,'code' => 'SHEKHBADR'],
            ['id' => 69,'name' => 'بانياس','city_id' => 14,'code' => 'BANEAS'],
            ['id' => 70,'name' => 'صافيتا','city_id' => 14,'code' => 'SAFETA'],
        ];
        DB::table('areas')->insert($areas);
    }
}
