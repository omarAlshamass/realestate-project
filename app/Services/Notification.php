<?php

namespace App\Services;

class Notification
{

    public function send($title, $body, $tokenIds = [])
    {

        $SERVER_API_KEY = 'AAAACo4v9gY:APA91bEYgWQf3An7oc_6ZK95CsHx5kSt-ADngkD-Mt7vCA9Fmwj049iuUBZF41yExfi3smJlbweFhXidPzq0Ru-eRdN6RLNRTHtHC_i6LCJCvZs_DXAy0KAMXNJHMgc9qCM7HQAv2NPJ';
        $data = [
            "registration_ids" => $tokenIds,
            "notification" => [
                "title" => $title,
                "body" => $body,
                "sound" => "default" // required for sound on ios
            ],
        ];

        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

        dd($response);
    }
}
