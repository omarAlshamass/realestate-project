<?php

namespace App\Repositories;

use App\Models\Estate;

class EstateRepository
{
    private $model;

    public function __construct(Estate $estate)
    {
        $this->model = $estate;
    }

    public function pending()
    {
        return $this->model->where('is_active', null);
    }

    public function approved()
    {
        return $this->model->where('is_active', 1);
    }

    public function rejected()
    {
        return $this->model->where('is_active', 0);
    }
}