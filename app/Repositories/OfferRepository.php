<?php

namespace App\Repositories;

use App\Models\Offer;

class OfferRepository
{
    private $model;

    public function __construct(Offer $model)
    {
        $this->model = $model;
    }

    public function addOffer($data)
    {
        $offer = new Offer();
        $offer->user_id = data_get($data, 'user_id');
        $offer->description = data_get($data, 'description');
        $offer->estate_id = data_get($data, 'estate_id');

        return $offer->save();
    }

    public function getAllByEstate($estateId)
    {
        return $this->model->where('estate_id', $estateId);
    }
}