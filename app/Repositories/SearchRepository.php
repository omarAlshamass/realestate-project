<?php

namespace App\Repositories;

use App\Models\Estate;

class SearchRepository
{
    private $model;

    public function __construct(Estate $estate)
    {
        $this->model = $estate;
    }

    public function estateSearch($data)
    {
        return $this->model
            ->where([
                ['price', '>=', $data['min_price']],
                ['price', '<=', $data['max_price']],
                ['area_id', $data['area_id']],
                ['rent_or_sale', $data['rent_or_sale']],
                ['realEstateType_id', $data['realEstateType_id']]
            ])->get();
    }
}