<?php

namespace App\Traits;

trait ResponseTrait
{
    private $responseFormat = [
        'data' => null,
        'errors' => null,
        'status' => false
    ];

    public function response($data = null, $errors = null, $code = 200)
    {
        $this->responseFormat['data'] = $data;
        $this->responseFormat['errors'] = $errors;
        $this->responseFormat['status'] = empty($errors) ? true : false;

        return  response($this->responseFormat, $code);
    }
}