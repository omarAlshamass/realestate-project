<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $with = ["user"];
    
    public $timestamps = true;

    public function estate()
    {
        return $this->belongsTo(Estate::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
