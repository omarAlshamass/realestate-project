<?php

namespace App\Http\Requests;

use App\Models\User;
use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:8',
            'phone_number'=>'required|unique:users',
        ];
    }

    public function messages()
    {
        return[
            'name.required'=>'اسم المستخدم مطلوب',
            'email.required'=>'البريد الإلكتروني مطلوب',
            'email.email'=>'البريد الإلكتروني الذي أدخلته غير صالح',
            'email.unique'=>'البريد الإلكتروني مستخدم',
            'password.required'=>'أدخل كلمة السر',
            'password.min'=>'يجب أن تكون كلمة السر أكثر من 8 أحرف',
            'phone_number.required'=>'أدخل رقم الموبايل',
            'phone_number.unique'=>'رقم الموبايل مستخدم',
        ];
    }
}
