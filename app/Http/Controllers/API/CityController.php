<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\City\CityResource;
use App\Models\City;
use App\Services\Notification;

class CityController extends Controller
{
    public function getAllCities(Notification $notification)
    {
        //$notification->send("title 1", "description 1", ["cqnhI35sS-24uQX7pmdGLY:APA91bGCSOATLSc7f4ooLSw2piMSkB0X0hTwzdvU3FzZ4G9quES54dIE1NS40OJahNZhRHpYSdFb1ccZGUtqM5lx48JatWFPuN87YESchvRwjCW-53M9exJ9YWAEEr20iI1V9ZjJ5eoh"]);
        return new CityResource(City::all());
    }
}
