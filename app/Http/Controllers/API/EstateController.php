<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\RealEstate\RealEstateResource;
use App\Http\Resources\RealEstate\RealEstatesResource;
use App\Http\Resources\User\UsersResource;
use App\Models\Estate;
use App\Models\Image;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class EstateController extends Controller
{
    /**
     * @param Request $request
     * @return RealEstatesResource
     */
    public function index(Request $request)
    {
        $estates = Estate::query()
            ->where('realEstateType_id', $request->id)
            ->where('is_active', 1)
            ->get();

        return new RealEstatesResource($estates);
    }

    public function all()
    {
        $estate = Estate::all();
        return new RealEstatesResource($estate);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validationRules = [
            'rent_or_sale' => 'boolean',

            'price' => 'required|numeric',
            'space' => 'required|numeric',

            'location_description' => 'required',

            'x_latitude' => 'required',
            'y_longitude' => 'required',

            'Specifications' => 'required',
            //foreign
            'area_id' => 'required',
            'user_id' => 'required',
            'realEstateRegistry_id' => 'required',
            'realEstateType_id' => 'required',

            'images' => 'required',
        ];

        $request->validate($validationRules);
        $idTypeRealEstate = $request->get('realEstateType_id');
        $infoTableType = DB::table('real_estate_types')->where('id', $idTypeRealEstate)->first()->code;
        if ($infoTableType) {
            switch ($infoTableType) {
                case "HOME":
                    $validationRules["livery.number_of_room"] = "required";
                    break;
                case "SHOP":
                    $validationRules["roof_shed"] = "required|integer|min:0";
                    break;
                case "LAND":
                    $validationRules["bear.inviolable.room"] = "required";
                    break;
            }
        }
        $user = (int)$id;
        $realEstate = new \App\Models\Estate();
        $realEstate->rent_or_sale =         $request->get('rent_or_sale');
        $realEstate->number_month =         $request->get('number_month');
        $realEstate->space =                $request->get('space');
        $realEstate->price =                $request->get('price');
        $realEstate->location_description = $request->get('location_description');
        $realEstate->specifications =       $request->get('Specifications');
        $realEstate->x_latitude =           $request->get('x_latitude');
        $realEstate->y_longitude =          $request->get('y_longitude');
        $realEstate->area_id =              $request->get('area_id');
        $realEstate->realEstateRegistry_id = $request->get('realEstateRegistry_id');
        $realEstate->realEstateType_id =    $request->get('realEstateType_id');
        $realEstate->user_id =              $user;

        $realEstate->save();

        //save multi image
        foreach ($request->images as $image) { // these arguments is not valid
            $insert = [];
            $imageChunks = explode(';', $image);
            $mimeType = $imageChunks[0];

            $imageExtension = 'jpeg';

            switch ($mimeType) {
                case "data:image/jpeg":
                    $imageExtension = "jpeg";
                    break;
                case "data:image/png":
                    $imageExtension = "png";
                    break;
                default:
                    $imageExtension = "jpeg";
                    break;
            }
            $imageName = str_random(10) . '.' . $imageExtension;
            Storage::disk('local')->put($imageName, base64_decode(str_replace($mimeType . ';base64,', '', $image)));
            $insert =
                [
                    'url' => $imageName,
                    'estate_id' => $realEstate->id
                ];
            Image::insert($insert);
        }
        return new RealEstateResource($realEstate);
    }

    /**
     * @param $id
     * @return RealEstateResource
     */
    public function show($id)
    {
        $estate = Estate::find($id);
        return new RealEstateResource($estate);
    }

    public function myRealEstate($id)
    {
        $estates = Estate::query()->where('user_id', $id)->get();
        return new RealEstatesResource($estates);
    }

}
