<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Area\AreaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AreaController extends Controller
{
    //get name areas from id city
    public function getAllAreas(Request $request){
        $areas = DB::table('areas')->where('city_id',$request->id)->get();
        return new AreaResource($areas);
    }
}
