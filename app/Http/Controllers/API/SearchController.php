<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\SearchRepository;
use App\Traits\ResponseTrait;

class SearchController extends Controller
{
    use ResponseTrait;

    private $searchRepository;

    public function __construct(SearchRepository $searchRepository)
    {
        parent::__construct();
        $this->searchRepository = $searchRepository;
    }

    public function index()
    {
        return $this->response($this->searchRepository->estateSearch(request()->all()));
    }
}