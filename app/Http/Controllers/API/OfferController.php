<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\OfferRepository;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OfferController extends Controller
{
    use ResponseTrait;

    private $offerRepositiory;

    public function __construct(OfferRepository $offerRepositiory)
    {
        parent::__construct();

        $this->offerRepositiory = $offerRepositiory;
    }

    public function index()
    {
        return $this->response($this->offerRepositiory->addOffer(request()->all()));
    }

    public function getByEstate($estateId)
    {
        return $this->response($this->offerRepositiory->getAllByEstate($estateId)->get());
    }
}
