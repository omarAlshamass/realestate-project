<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\City;
use App\Models\Estate;
use App\Models\Image;
use App\Models\RealEstateType;
use App\Models\RealEstateRegistry;
use App\Repositories\EstateRepository;
use Illuminate\Support\Facades\DB;

class EstateController extends Controller
{
    private $estateRepository;

    public function __construct(EstateRepository $estateRepository)
    {
        $this->estateRepository = $estateRepository;
    }

    public function getApproved()
    {
        $estates =  $this->estateRepository->approved()->get();
        
        return view('admin.estate.getApproved',['estates'=>$estates]);
    }

    public function getRejected()
    {
        $estates =  $this->estateRepository->rejected()->get();
        return view('admin.estate.getRejected',['estates'=>$estates]);
    }

    public function getPending()
    {
        $estates =  $this->estateRepository->pending()->get();
        return view('admin.estate.getPending',['estates'=>$estates]);
    }

    public function show($estateId)
    {
        $estate=Estate::find($estateId);
        $area=Area::where('id','=',$estate->area_id)->first();
        $city=City::where('id','=',$area->city_id)->first();
        $type=RealEstateType::where('id','=',$estate->realEstateType_id)->first();
        $images=Image::where('estate_id',$estate->id)->get();
        $register=RealEstateRegistry::where('id',$estate->realEstateRegistry_id)->first();
        return view('admin.estate.show',['estate'=>$estate,'city'=>$city,'area'=>$area,'type'=>$type,'images'=>$images,'register'=>$register]);
    }

    public function setAccept($estateId){
        Estate::find($estateId)->update([
            'is_active'=>1,
        ]);
        return redirect()->route('estate.approved');
    }

    public function setNotAccept($estateId){
        Estate::find($estateId)->update([
            'is_active'=>0,
        ]);
        return redirect()->route('estate.approved');
    }
}