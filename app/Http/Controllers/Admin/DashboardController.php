<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
//############### Fun : Show view Home Admin ################
    public function index(){
            return view('layouts.admin.dashboard');
    }
}
