<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    //show set all data users in site
    public function index($order, Request $request)
    {
        //search from record the users by field username
        if (request()->has('field_search') and request('field_search') != "") {
            $filterUser = User::where('name', 'like', '%' . request()->get('field_search') . '%')->get()->first();
            if ($filterUser)
                return view('admin.users.index', ['filterUser' => $filterUser]);
            else
                return redirect()->route('users.index', 'asc')->with('msg', 'لا يوجد حقل بهذا الاسم');
        }
        //for show records by order
        if ($order == 'asc')
            $users = User::orderBy('name')->paginate(10); //'desc' order records reverse
        else if ($order == 'desc') {
            $users = User::orderBy('name', 'desc')->paginate(10);
        } else
            $users = User::paginate(10);
        return view('admin.users.index', ['users' => $users]);
    }

    //Show the form for creating the data in DB.
    public function create()
    {
        return view('admin.users.create');
    }

    //storage data in DB
    public function store(UserRequest $request)
    {
        User::create(array_merge(
            $request->all(),
            [
                'password' => Hash::make($request->password),
                'remember_token' => Hash::make(Str::random(10)),
            ]
        ));
        return redirect()->route('users.index', 'تصاعدي')->with('msg', 'تم إنشاء سجل المستخدم بنجاح');
    }

    //Show the form for editing the data in DB.
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', ['user' => $user]);
    }

    //Update the Data in DB.
    public function update(Request $request, $id)
    {
        User::find($id)->update(array_merge($request->all(), ['password' => Hash::make($request->password)]));
        return redirect()->route('users.index', 'asc')->with('msg', 'تم تعديل سجل المستخدم بنجاح');
    }
    //Remove the Data in DB.
    public function destroy($id)
    {
        $user = User::find($id)->delete();
        return redirect()->route('users.index', 'asc')->with('msg', 'تم حذف سجل المستخدم بنجاح');
    }
}
