<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $credentials['is_admin'] = 1;

        if (Auth::guard('user')->attempt($credentials)) //default guard user
        {
            $request->session()->put('is_admin', true);

            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login')->with('failsLogin', 'بيانات الدخول غير صحيحة');
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('user')->logout();
        $request->session()->forget('is_admin');

        return redirect()->route('login');
    }
}