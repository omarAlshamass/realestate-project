<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class authenticatedUser
{
    //to check from is admin
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            return $next($request);
        } else abort(404);
    }
}